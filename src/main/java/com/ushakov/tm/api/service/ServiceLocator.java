package com.ushakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ServiceLocator {

    @NotNull
    IAuthService getAuthService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

}
