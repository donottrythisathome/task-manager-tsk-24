package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.IProjectRepository;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class ProjectRepository extends AbstractOwnerBusinessRepository<Project> implements IProjectRepository {
}