package com.ushakov.tm.service;

import com.ushakov.tm.api.service.IAuthService;
import com.ushakov.tm.api.service.IUserService;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.exception.empty.EmptyLoginException;
import com.ushakov.tm.exception.empty.EmptyPasswordException;
import com.ushakov.tm.exception.entity.UserNotFoundException;
import com.ushakov.tm.exception.auth.AccessDeniedException;
import com.ushakov.tm.model.User;
import com.ushakov.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void checkRoles(@Nullable final Role... roles) {
        if (roles == null || roles.length == 0) return;
        @Nullable final User user = getUser();
        if (user == null) throw new AccessDeniedException();
        @NotNull final Role role = user.getRole();
        for (@Nullable final Role item: roles) {
            if (role.equals(item)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    @Nullable
    public User getUser() {
        @NotNull final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @Override
    @NotNull
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = userService.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (user.getLocked()) throw new AccessDeniedException();
        @NotNull final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void registry(@NotNull final String login, @NotNull final String password, @Nullable final String email) {
        userService.add(login, password, email);
    }

}
