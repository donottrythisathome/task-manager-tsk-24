package com.ushakov.tm.service;

import com.ushakov.tm.api.IRepository;
import com.ushakov.tm.api.IService;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import com.ushakov.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    final private IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    @NotNull
    public E add(@NotNull final E entity) {
        return repository.add(entity);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    @Nullable
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    @Nullable
    public E findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findOneById(id);
    }

    @Override
    public void remove(@Nullable final E entity) {
        if (entity == null) throw new ObjectNotFoundException();
        repository.remove(entity);
    }

    @Override
    @Nullable
    public E removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeOneById(id);
    }

}
